--[[
X-Plane uses on,off or toggle as configureable items

Below are some joystick switches, which toggle automatically to off
when switch is NOT on

It's a workaround using FlyWithLua
--]]

button_debug_string2 = "No button pressed!"
button_sniffer_active2 = true

function button_sniffer_event_handler2()
	if button_sniffer_active2 then
            if not last_button(800) and button(800) then
                button_debug_string2 = "Fuel pump on."
				set ("sim/cockpit2/switches/electric_hydraulic_pump_on",1)
            end
            if last_button(800) and not button(800) then
                button_debug_string2 = "Fuel pump off."
				set ("sim/cockpit2/switches/electric_hydraulic_pump_on",0)
            end
			if not last_button(801) and button(801) then
                button_debug_string2 = "Beacon on."
				set ("sim/cockpit/electrical/beacon_lights_on",1)
            end
            if last_button(801) and not button(801) then
                button_debug_string2 = "Beacon off."
				set ("sim/cockpit/electrical/beacon_lights_on",0)
            end
			if not last_button(802) and button(802) then
                button_debug_string2 = "Land on."
				set ("sim/cockpit/electrical/landing_lights_on",1)
            end
            if last_button(802) and not button(802) then
                button_debug_string2 = "Land off."
				set ("sim/cockpit/electrical/landing_lights_on",0)
            end
			if not last_button(803) and button(803) then
                button_debug_string2 = "Taxi off."
				set ("sim/cockpit/electrical/taxi_light_on",1)
            end
            if last_button(803) and not button(803) then
                button_debug_string2 = "Taxi off."
				set ("sim/cockpit/electrical/taxi_light_on",0)
            end
			if not last_button(804) and button(804) then
                button_debug_string2 = "Nav on."
				set ("sim/cockpit/electrical/nav_lights_on",1)
            end
            if last_button(804) and not button(804) then
                button_debug_string2 = "Nav off."
				set ("sim/cockpit/electrical/nav_lights_on",0)
            end
			if not last_button(805) and button(805) then
                button_debug_string2 = "Strobe on."
				set ("sim/cockpit/electrical/strobe_lights_on",1)
            end
            if last_button(805) and not button(805) then
                button_debug_string2 = "Strobe off."
				set ("sim/cockpit/electrical/strobe_lights_on",0)
            end
			if not last_button(806) and button(806) then
                button_debug_string2 = "Pitot on."
				set ("sim/cockpit2/switches/pitot_heat_on",1)
            end
            if last_button(806) and not button(806) then
                button_debug_string2 = "Pitot off."
				set ("sim/cockpit2/switches/pitot_heat_on",0)
            end
			if not last_button(807) and button(807) then
                button_debug_string2 = "Master Alt on."
				set ("sim/cockpit/electrical/generator_on",1)
            end
            if last_button(807) and not button(807) then
                button_debug_string2 = "Master Alt off."
				set ("sim/cockpit/electrical/generator_on",0)
            end
			if not last_button(808) and button(808) then
                button_debug_string2 = "Master Batt on."
				set ("sim/cockpit/electrical/battery_on",1)
            end
            if last_button(808) and not button(808) then
                button_debug_string2 = "Master Batt off."
				set ("sim/cockpit/electrical/battery_on",0)
            end
    end
end

do_every_frame("button_sniffer_event_handler2()")

function print_button_sniffer_result2()
	if button_sniffer_active2 then
		draw_string(50, 850, button_debug_string2)
	end
end

do_every_draw("print_button_sniffer_result2()")
add_macro("Show nav toggle joystick button numbers", "button_sniffer_active2 = true", "button_sniffer_active2 = false", "activate")
